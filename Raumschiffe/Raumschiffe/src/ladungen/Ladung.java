package ladungen;

public class Ladung {
	
	//Attribute
	private String bezeichnung;
	private int anzahl;
	
	//Konstruktoren
	public Ladung() {
		
	}

	public Ladung(String bezeichnung, int anzahl) {
		this.bezeichnung = bezeichnung;
		this.anzahl = anzahl;
	}

	
	//Getter und Setter
	public String getBezeichnung() {
		return bezeichnung;
	}

	public void setBezeichnung(String bezeichnung) {
		this.bezeichnung = bezeichnung;
	}

	public int getAnzahl() {
		return anzahl;
	}

	public void setAnzahl(int anzahl) {
		this.anzahl = anzahl;
	}

	//toString
	@Override
	public String toString() {
		return "CName " + Ladung.class.getName() + "\nBezeichnung " + this.getBezeichnung() + "\nAnzahl " + this.getAnzahl();
	}
	
	
	//Methoden
	
	
}
