package raumschiffeRunner;

import java.util.ArrayList;

import ladungen.Ladung;
import raumschiffe.Raumschiff;

public class RaumschiffeRunner {

	public static void main(String[] args) {
		
		Raumschiff klingonen = new Raumschiff("IKS HEgh'ta", 100, 100, 100, 100,
			1, 2, new ArrayList<Ladung>(), new ArrayList<String>());
		
		Raumschiff romulaner = new Raumschiff("IRW Khazara", 100, 100, 100, 100,
				2, 2, new ArrayList<Ladung>(), new ArrayList<String>());
		
		Raumschiff vulkanier = new Raumschiff("Ni'Var", 80, 80, 100, 50,
				0, 5, new ArrayList<Ladung>(), new ArrayList<String>());
		
		
		klingonen.addLadung(new Ladung("Ferengi Schneckensaft", 200));
		klingonen.addLadung(new Ladung("Bat'leth Klingonen Schwert", 200));
		
		romulaner.addLadung(new Ladung("Borg-Schrott", 5));
		romulaner.addLadung(new Ladung("Rote Materie", 2));
		romulaner.addLadung(new Ladung("Plasma-Waffe", 50));
		
		vulkanier.addLadung(new Ladung("Forschungssonde", 35));
		vulkanier.addLadung(new Ladung("Photonentorpedo", 3));
		
		klingonen.torpedoFeuern(romulaner);
		romulaner.phaserkanoneFeuern(klingonen);
		
		vulkanier.nachrichtAnAlle("Gewalt ist nicht logisch");
		
		klingonen.zustandAusgeben();
		klingonen.ladungsVerzeichnisAusgeben();
		
		vulkanier.reparaturAndroidenEinsetzen(2, true, true, true);
		vulkanier.photonentorpedosLaden(2);
		vulkanier.ladungsVerzeichnisAufraeumen();
		
		klingonen.torpedoFeuern(romulaner);
		klingonen.torpedoFeuern(romulaner);
		
		klingonen.zustandAusgeben();
		vulkanier.zustandAusgeben();
		romulaner.zustandAusgeben();
		
		Raumschiff.logbuchAusgabe();
		
		
		
	}

}
