package raumschiffe;

import java.util.ArrayList;

import ladungen.Ladung;

/**
 * 
 * @author nestl
 * @since 01.06.2022
 * @version 1.0
 */
public class Raumschiff {
	
	//Attribute
	private String name;
	private int energieVersorgung;
	private int schutzschild;
	private int lebenserhalt;
	private int huelle;
	private int photonenTorpedo;
	private int reperaturAndroiden;
	private ArrayList<Ladung> ladungsVerzeichnis = new ArrayList<>();
	private static ArrayList<String> broadcastKommunikator = new ArrayList<>();
	
	//Konstruktoren
	public Raumschiff() {
		
	}

	public Raumschiff(String name, int energieVersorgung, int schutzschild, int lebenserhalt, int huelle,
			int photonenTorpedo, int reperaturAndroiden, ArrayList<Ladung> ladungsVerzeichnis,
			ArrayList<String> broadcastKommunikator) {
		this.name = name;
		this.energieVersorgung = energieVersorgung;
		this.schutzschild = schutzschild;
		this.lebenserhalt = lebenserhalt;
		this.huelle = huelle;
		this.photonenTorpedo = photonenTorpedo;
		this.reperaturAndroiden = reperaturAndroiden;
		this.ladungsVerzeichnis = ladungsVerzeichnis;
		this.broadcastKommunikator = broadcastKommunikator;
	}
	
	//Getter und Setter
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getEnergieVersorgung() {
		return energieVersorgung;
	}

	public void setEnergieVersorgung(int energieVersorgung) {
		this.energieVersorgung = energieVersorgung;
	}

	public int getSchutzschild() {
		return schutzschild;
	}

	public void setSchutzschild(int schutzschild) {
		this.schutzschild = schutzschild;
	}

	public int getLebenserhalt() {
		return lebenserhalt;
	}

	public void setLebenserhalt(int lebenserhalt) {
		this.lebenserhalt = lebenserhalt;
	}

	public int getHuelle() {
		return huelle;
	}

	public void setHuelle(int huelle) {
		this.huelle = huelle;
	}

	public int getPhotonenTorpedo() {
		return photonenTorpedo;
	}

	public void setPhotonenTorpedo(int photonenTorpedo) {
		this.photonenTorpedo = photonenTorpedo;
	}

	public int getReperaturAndroiden() {
		return reperaturAndroiden;
	}

	public void setReperaturAndroiden(int reperaturAndroiden) {
		this.reperaturAndroiden = reperaturAndroiden;
	}

	public ArrayList<Ladung> getLadungsVerzeichnis() {
		return ladungsVerzeichnis;
	}

	public void setLadungsVerzeichnis(ArrayList<Ladung> ladungsVerzeichnis) {
		this.ladungsVerzeichnis = ladungsVerzeichnis;
	}

	public ArrayList<String> getBroadcastKommunikator() {
		return broadcastKommunikator;
	}

	public void setBroadcastKommunikator(ArrayList<String> broadcastKommunikator) {
		this.broadcastKommunikator = broadcastKommunikator;
	}
	
	//toString
	@Override
	public String toString() {
		return "Raumschiff [name=" + name + ", energieVersorgung=" + energieVersorgung + ", schutzschild="
				+ schutzschild + ", lebenserhalt=" + lebenserhalt + ", huelle=" + huelle + ", photonenTorpedo="
				+ photonenTorpedo + ", reperaturAndroiden=" + reperaturAndroiden + "]";
	}
	
	
	//Methoden
	
	//Beispielannotation
	/**Voraussetzung.: Falls das angegebene Konto existiert,
	* (Effekt.:) wird der Betrag von diesem Konto abgehoben.
	* Andernfalls geschieht nichts. Nutzt abheben( ) aus Konto.
	* @param inhaberKontonummer die Kontonummer des Empfängers
	* @param betrag der abzuhebende Betrag
	* @return true, falls die Abhebung ausgeführt werden konnte, sonst false */
	
/**	Effekt: Eine neue Ladung wir dem Ladungsverzeichnis hinzugefügt
	@param naueLadung die hinzuzufügende Ladung 
*/
	public void addLadung(Ladung neueLadung) {
		this.ladungsVerzeichnis.add(neueLadung);
	}

	
/** Effekt: Das Ladungsverzeichnis wird auf der Konsole ausgegeben
 */
	public void ladungsVerzeichnisAusgeben() {
		getLadungsVerzeichnis().forEach(n -> System.out.println(n.toString()));
	}

/** Effekt: Alle Attribute des Objektes werden mit zugehörigen Werten auf der Konsole ausgegeben
 */
	public void zustandAusgeben() {
		System.out.println(this.toString());
	}

/** Effekt: Eine neue Nachrichtwird dem broadcastKommunikator als string hinzugefügt	
 * @param nachricht die hinzuzufügende Nachricht
 */
	public void nachrichtAnAlle(String nachricht) {
		broadcastKommunikator.add(nachricht);
	}
	
/** Effekt: Alle im broadcastKommunikator gespeicherte Nachrichten werden auf der Konsole ausgegeben
 */
	public static void logbuchAusgabe() {
		broadcastKommunikator.forEach(n -> System.out.println(n));
	}
	
/** Effekt: Falls ein Photonentorpedo geladen ist, wird dieser auf ein anderes Raumschiff abgefeuert
 *  Wenn keiner vohanden sein sollte wird dem broadcastKommunikator "-=*Click*=-" übergeben
 * 	
 * @param zielRaumschiff das Objekt der Klasse Raumschiff auf welches ein Torpedo abgefeuert werden soll
 */
	public void torpedoFeuern(Raumschiff zielRaumschiff) {
		if(this.photonenTorpedo > 0) {
			this.photonenTorpedo -= 1;
			nachrichtAnAlle("Photonentorpedo Abgeschossen");
			trefferVermerken(zielRaumschiff);
		} else {
			nachrichtAnAlle("-=*Click*=-");
		}
	}
	
/** Effekt: Falls genügend Energie vorhanden ist, wird die Phaserkanone auf ein anderes Raumschiff gefeuert
 *  Wenn zu wenig vohanden sein sollte wird dem broadcastKommunikator "-=*Click*=-" übergeben
 * 
 * @param zielRaumschiff das Objekt der Klasse Raumschiff auf welches die Kanone abgefeuert werden soll
 */
	public void phaserkanoneFeuern(Raumschiff zielRaumschiff) {
		if(this.energieVersorgung >= 50) {
			this.energieVersorgung -= 50;
			nachrichtAnAlle("Phaserkanone Abgeschossen");
			trefferVermerken(zielRaumschiff);
		} else {
			nachrichtAnAlle("-=*Click*=-");
		}
	}
	
/** Effekt: Dem Broadcastkommunikator wird der Name des Zielraumschiffs und "wurde getroffen" übergeben 
 * 	Falls das übergebene Raumschiff mehr als 50% Schilde hat, werden diese um 50% verringert
 * 	Falls das übergebene Raumschiff weniger als 50% Schilde hat werden die Hülle und energieversorgung um 50% verringert
 * 	Falls danach die Hülle kleiner oder gleich Null ist wird dem Broadcastkommunikator "Lebenserhaltungssystheme zerstört" übergeben
 * 
 * @param zielRaumschiff das Objekt der Klasse Raumschiff welches getroffen werden soll
 */
	public void trefferVermerken(Raumschiff zielRaumschiff) {
		System.out.println(zielRaumschiff.getName() + " wurde getroffen");
		zielRaumschiff.schutzschild -= 50;
		if(zielRaumschiff.schutzschild <= 0) {
			zielRaumschiff.huelle -= 50;
			zielRaumschiff.energieVersorgung -= 50;
			zielRaumschiff.setSchutzschild(0);
			if(zielRaumschiff.getHuelle() <= 0) {
				nachrichtAnAlle("Lebenserhaltungssystheme zerstört");
			}
		}
	}
	
/** Effekt: Torpedos werden falls im Ladungsverzeichnis vorhanden den geladenen Torpedos im Attribut photonenTropedos hinzugefügt
 *  Falls keine Torpedos im Ladungsverzeichnis sind wird "Keine Photonentorpedos gefunden!" auf der Konsole ausgegeben
 *  Falls nicht genügend Torpedos im Ladungsverzeichnis sind werden alle möglichen hinzugefügt
 *  Falls  genügend Torpedos im Ladungsverzeichnis sind werden alle geforderten hinzugefügt
 * 
 * @param torpedoAnzahl die Anzahl der Torpedos die zum Attribut torpedoAnzahl hinzugefügt werden sollen
 */
	public void photonentorpedosLaden(int torpedoAnzahl) {
		int geladeneTorpedos = 0;
		for(int i = 0; i < this.ladungsVerzeichnis.size(); i++) {
			if(this.ladungsVerzeichnis.get(i).getBezeichnung() == "photonenTorpedos");
				geladeneTorpedos += this.ladungsVerzeichnis.get(i).getAnzahl();
		}
		if(geladeneTorpedos == 0) {		
			System.out.println("Keine Photonentorpedos gefunden!");
			nachrichtAnAlle("-=*Click*=-");
		} else {
			if(geladeneTorpedos <= torpedoAnzahl) {
				for(int i = 0; i < ladungsVerzeichnis.size(); i++) {
					if(ladungsVerzeichnis.get(i).getBezeichnung() == "photonenTorpedos");
					ladungsVerzeichnis.get(i).setAnzahl(0);
				}
				System.out.println("[" + geladeneTorpedos + "] Photonentorpedo(s) eingesetzt");
				ladungsVerzeichnisAufraeumen();
			}
			if(geladeneTorpedos > torpedoAnzahl) {
				for(int i = 0; i < ladungsVerzeichnis.size(); i++) {
					if(ladungsVerzeichnis.get(i).getBezeichnung() == "photonenTorpedos");
					ladungsVerzeichnis.get(i).setAnzahl(geladeneTorpedos - torpedoAnzahl);
				}
				System.out.println("[" + torpedoAnzahl + "] Photonentorpedo(s) eingesetzt");
			}
		}
		
	}
	
/** Effekt: Objekte von welchen das Attribut Anzahl Null ist werden aus dem Ladungsverzeichnis gelöscht
 * 
 */
	public void ladungsVerzeichnisAufraeumen() {
		for(int i = 0; i < ladungsVerzeichnis.size(); i++) {
			if(ladungsVerzeichnis.get(i).getAnzahl() == 0) {
				ladungsVerzeichnis.remove(i);
			}
		}
	}

/** Effekt: Die übergebene Systeme werden falls möglich unter verwendung der gewünschten Zahla an droiden repariert
 *  Falls die vorhandene Droidenzahl größer der gewünschten ist werden alle gewünschten verwendet
 *  Falls die vorhandene Droidenzahl kleiner der gewünschten ist werden alle vorhanden verwendet
 * 
 * @param droidenAnzahl die Anzahl der zur Reparatur gewünschten Droiden
 * @param schutzschild ob das Schild repariert werden soll
 * @param lebenserhalt ob die Lebenserhaltungssytheme repariert werden sollen
 * @param huelle ob die Hülle repariert werden soll
 */
	public void reparaturAndroidenEinsetzen(int droidenAnzahl, boolean schutzschild, boolean lebenserhalt, boolean huelle) {
		double zufallsZahl = Math.random() * 100;
		int aufTrueGesetzt = 0;
		if(schutzschild == true) aufTrueGesetzt += 1;
		if(lebenserhalt == true) aufTrueGesetzt += 1;
		if(huelle == true) aufTrueGesetzt += 1;
		int verwendeteDroiden = droidenAnzahl;
		if(droidenAnzahl > this.reperaturAndroiden) {
			verwendeteDroiden = this.reperaturAndroiden;
		}
		
		double ergebnis = (zufallsZahl * verwendeteDroiden) / aufTrueGesetzt;
		
		if(schutzschild == true) {
			this.schutzschild += ergebnis;
		}
		if(lebenserhalt == true) {
			this.lebenserhalt += ergebnis;
		}
		if(huelle == true) {
			this.huelle += ergebnis;
		}
	}
	
	
	
}
