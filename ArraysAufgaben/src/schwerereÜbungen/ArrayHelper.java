package schwerereÜbungen;

public class ArrayHelper {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int[] zufälligesArray = new int[10];
		for (int i = 0; zufälligesArray.length > i; i++) {
			zufälligesArray[i] = (int) (Math.random() * 100);
		}
		
		System.out.println(convertArrayToString(zufälligesArray));
		//System.out.println(convertArrayToString(arrayVertauscher(zufälligesArray)));
		System.out.println(convertArrayToString(arrayZuArrayVertauscher(zufälligesArray)));
		
	}
	
	public static String convertArrayToString (int[] zahlen) {
		StringBuffer sb = new StringBuffer();
		 for(int i = 0; i < zahlen.length; i++) {
	         sb.append(zahlen[i]);
	         if(i + 1 < zahlen.length) {
		         sb.append(", ");
	         }
	      }
		 String str = sb.toString();
		 return str;
	}
	
	public static int[] arrayVertauscher(int [] zahlen) {
		for(int i = 0; i < zahlen.length / 2; i++) {
			int j = zahlen[i];
			zahlen[i] = zahlen[zahlen.length - 1 - i];
			zahlen[zahlen.length - 1 - i] = j;
		}
		return zahlen;
	}
	
	public static int[] arrayZuArrayVertauscher(int [] zahlen) {
		
		int[] zurück = new int[zahlen.length];
		
		for(int i = 0; i < zahlen.length; i++) {
			zurück[zahlen.length - 1 - i] = zahlen[i];
		}
		return zurück;
	}
	
//	public static double[][] fahrenheitZuCelsius(double zweiD[][]) {
//		
//	}

}