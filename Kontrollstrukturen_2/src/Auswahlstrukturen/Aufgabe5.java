package Auswahlstrukturen;

import java.util.Scanner;

public class Aufgabe5 {
	
	static Scanner eingabe = new Scanner(System.in);

	public static void main(String[] args) {
		
		
		double größe = maße("Bitte geben Sie Ihre Größe in cm ein") / 100;
		double gewicht = maße("Bitte geben Sie Ihr Gewicht in kg ein");
		String sex = geschlecht("Bitte geben Sie Ihr Geschlecht an [m/w]");
		double bmi = errechneBmi(größe, gewicht);
		String ausgabe = ausgabeDesBmi(sex, bmi);
		System.out.println(ausgabe);
		
		eingabe.close();
	}
	
	public static double maße (String eingabeAufforderung) {
		System.out.println(eingabeAufforderung);
		return eingabe.nextDouble();
	}
	
	public static String geschlecht(String eingabeAufforderung) {
		System.out.println(eingabeAufforderung);
		return eingabe.next();
	}

	public static double errechneBmi(double größe, double gewicht) {
		return gewicht/(größe * 2);
	}
	
	public static String ausgabeDesBmi(String sex, double bmi) {
		String ausgabe = "";
		if (sex.contains("m")){
		      if (bmi<20){
		        ausgabe="Sie haben Untergewicht";
		      }
		      else if(bmi>=20 && bmi<=25){
		        ausgabe="Sie haben Normalgewicht";
		      }
		      else{
		        ausgabe="Sie haben �bergewicht";
		      }
		    }
		    else if (sex.contains("w")){
		      if (bmi<19){
		        ausgabe="Sie haben Untergewicht";
		      }
		      else if(bmi>=19 && bmi<=24){
		        ausgabe="Sie haben Normalgewicht";
		      }
		      else{
		        ausgabe="Sie haben �bergewicht";
		      }
		    }
		    else{
		      ausgabe="Das ist kein Geschlecht";
		    }
		return ausgabe;
	}
	
}
