package Fallunterscheidungen;

import java.util.Scanner;


public class Aufgabe5 {
	
	static Scanner eingabe = new Scanner(System.in);
	
	public static void main(String[] args) {
		
		System.out.println("Was wollen Sie berechnen [R/I/U]?");
		char zuBerechnen = eingabe.next().charAt(0);
		berechnen(zuBerechnen);

	}
	
	
	public static void berechnen(char zuBerechnen) {
		double r = 0;
		double i = 0;
		double u = 0;
		switch (zuBerechnen)
		{
			case 'R':
			case 'r':
				System.out.println("Spannung in Volt eingeben: ");
				u = eingabe.nextDouble();
				System.out.println("Strom in Ampere eingeben: ");
				i = eingabe.nextDouble();
				r = u / i;
				System.out.printf("R = %.2f Ohm", r);
				break;
			case 'U':
			case 'u':
				System.out.println("Strom in Ampere eingeben: ");
				i = eingabe.nextDouble();
				System.out.println("Widerstand in Ohm eingeben: ");
				r = eingabe.nextDouble();
				u = i * r;
				System.out.printf("U = %.2f Volt", u);
				break;
			case 'I':
			case 'i':
				System.out.println("Spannung in Volt eingeben: ");
				u = eingabe.nextDouble();
				System.out.println("Widerstand in Ohm eingeben: ");
				r = eingabe.nextDouble();
				i = u / r;
				System.out.printf("I = %.2f Ampere", i);
				break;
			default:
				System.out.println("Falsche Auswahl!");
				break;
		}

	}

}
