package Fallunterscheidungen;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class Aufgabe6 {

	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);

        System.out.print("R�mische zahl: ");
        String romanNumber = sc.next();
        int result = romanToInteger(romanNumber);
        if (result != 0) {
            System.out.println("Its Integer Value is: " + result);
        } else {
            System.out.println("Not a roman number");
        }

        System.out.println();
        sc.close();


	}
	
	public static int romanToInteger(String roman) {

        boolean isValid = true;

        Map<Character, Integer> romanNums = new HashMap<>();
        romanNums.put('I', 1);
        romanNums.put('V', 5);
        romanNums.put('X', 10);
        romanNums.put('L', 50);
        romanNums.put('C', 100);
        romanNums.put('D', 500);
        romanNums.put('M', 1000);

        int result = 0;

        for (int i = 0; i < roman.length(); i++) {
            char ch = roman.charAt(i);
//        	Regeln:
//    		Zahlen nebeneinander addieren, kleinere Zahlen folgen gr��eren
//    		Maximal 3 gleiche Zeichen nebeneinander
//    		Wenn eine kleinere Zahl neben einer gr��eren steht wird es abgezogen
//    		Bei subtraktion: 	I nur von V abziehen
//    							X nur von L, C abziehen
//    							C nur von D, M abziehen
            if (roman.length() > 4) {
                if (ch == roman.charAt(i + 1) && ch == roman.charAt(i + 2) && ch == roman.charAt(i + 3)) {
                    isValid = false;
                    break;
                }
            }

            if (i + 1 < roman.length()) {
                if ((ch == 'I' && roman.charAt(i + 1) != 'V') && (ch == 'I' && roman.charAt(i + 1) != 'X')) {
                    isValid = false;
                    break;
                }
                if ((ch == 'X' && roman.charAt(i + 1) != 'L') && (ch == 'X' && roman.charAt(i + 1) != 'C')) {
                    isValid = false;
                    break;
                }
                if ((ch == 'C' && roman.charAt(i + 1) != 'D') && (ch == 'C' && roman.charAt(i + 1) != 'M')) {
                    isValid = false;
                    break;
                }
            }

            if (i > 0 && romanNums.get(ch) > romanNums.get(roman.charAt(i - 1))) {
                result += (romanNums.get(ch) - 2 * romanNums.get(roman.charAt(i - 1)));
            } else
                result += romanNums.get(ch);
        }

        if (isValid) {
            return result;
        } else {
            return 0;
        }
    }	
}
