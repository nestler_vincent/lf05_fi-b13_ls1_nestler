package Schleifen2;

import java.util.Scanner;

public class Aufgabe6 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner eingabe = new Scanner(System.in);
		million(eingabe);
		
	        
	}
	
	public static void million(Scanner eingabe) {
		
		int jahre = 0;
        float zinsSatz;
        float einzahlung;
        boolean weiter = true;

        while (weiter){
            System.out.print("Einzahlung: ");
            einzahlung = eingabe.nextFloat();
            System.out.print("\nZinssatz: ");
            zinsSatz = eingabe.nextFloat();

            while (einzahlung < 1000000){
                einzahlung = einzahlung * ((zinsSatz / 100) +1);
                jahre++;
            }

            System.out.print("In ");
            System.out.print(jahre);
            System.out.println(" Jahren bist du millionär 🥳");

            System.out.println("weiter? (j/n)");
            String jo = eingabe.next();
            if (jo.equals("n")){
                break;
            }

        }
	}
	
}
