package Schleifen2;

import java.util.Scanner;

public class Aufgabe8 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner eingabe = new Scanner(System.in);

		System.out.print("Bitte geben Sie eine Zahl zwischen 2 und 9 ein: ");
		int zahl = eingabe.nextInt();

		matrix(zahl);

		eingabe.close();
	}
	
	public static void matrix(int teilZahl) {
		
		int i = 0;
		while (i < 100) {
			
			int stelle1 = i / 10;
			int stelle2 = i % 10;
			
			
			if (i % teilZahl == 0) //pr�ft ob i durch die eingegebene Zahl teilbar ist
				System.out.printf("%4s", "*");
			
			else if ( stelle1 == teilZahl || stelle2 == teilZahl) // ziffer enthalten
				System.out.printf("%4s", "*");
			
			else if ( stelle1 + stelle2 == teilZahl) // quersumme
				System.out.printf("%4s", "*");
			
			else 
				System.out.printf("%4d", i);
			
			i++;
			
			if (i % 10 == 0 && i != 0) //neue zeile
				System.out.println();
	
		}
	}
	
}
