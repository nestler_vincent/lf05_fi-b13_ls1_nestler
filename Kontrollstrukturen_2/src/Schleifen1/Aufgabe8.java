package Schleifen1;

import java.util.Scanner;

public class Aufgabe8 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner eingabe = new Scanner(System.in);
				
			System.out.print("Geben Sie die Länge des Quadrates ein: ");
			int size = eingabe.nextInt() - 1;
	
			System.out.println("");
			
			for (int x = 0; x <= size; x++) {
				
				for (int y = 0; y <= size; y++) {
					
					if (x == 0 || x == size || y == 0 || y == size) {
						System.out.printf("%2s", "*");
					} 
					else {
						System.out.print("  ");
					}
				}
				System.out.print("\n");
			}
	
			
			eingabe.close();
		}

}
