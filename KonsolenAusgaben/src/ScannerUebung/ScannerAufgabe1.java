package ScannerUebung;

import java.math.BigDecimal;
import java.util.Scanner;

public class ScannerAufgabe1 {

	public static void main(String[] args) {
		
		// Neues Scanner-Objekt myScanner wird erstellt
		Scanner myScanner = new Scanner(System.in);
		System.out.print("Bitte geben Sie eine ganze Zahl ein: ");
		// Die Variable zahl1 speichert die erste Eingabe
		int zahl1 = myScanner.nextInt();
		System.out.print("Bitte geben Sie eine zweite ganze Zahl ein: ");
		// Die Variable zahl2 speichert die zweite Eingabe
		int zahl2 = myScanner.nextInt();
		// Die Addition der Variablen zahl1 und zahl2
		// wird der Variable ergebnis zugewiesen.
		int ergebnisAddition = zahl1 + zahl2;
		int ergebnisSubtraktion= zahl1 - zahl2;
		int ergebnisMultiplikation = zahl1 * zahl2;
		
		BigDecimal zaehler = new BigDecimal(zahl1);
		BigDecimal nenner = new BigDecimal(zahl2);
		BigDecimal ergebnisDivision = zaehler.divide(nenner);
		
		
		System.out.print("\nErgebnis der Addition lautet: ");
		System.out.print(zahl1 + " + " + zahl2 + " = " + ergebnisAddition);
		
		System.out.print("\nErgebnis der Subtraktion lautet: ");
		System.out.print(zahl1 + " - " + zahl2 + " = " + ergebnisSubtraktion);
		
		System.out.print("\nErgebnis der Addition lautet: ");
		System.out.print(zahl1 + " * " + zahl2 + " = " + ergebnisMultiplikation);
		
		System.out.print("\nErgebnis der Addition lautet: ");
		System.out.print(zahl1 + " / " + zahl2 + " = " + ergebnisDivision);
		
		myScanner.close();
	}

}
