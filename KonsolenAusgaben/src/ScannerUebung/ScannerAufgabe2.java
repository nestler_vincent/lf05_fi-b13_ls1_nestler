package ScannerUebung;

import java.util.Scanner;

public class ScannerAufgabe2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		// Neues Scanner-Objekt myScanner wird erstellt
		Scanner myScanner = new Scanner(System.in);
		System.out.print("Hallo, bitte geben Sie Ihren Namen ein: ");
		// Die Variable nutzerName speichert den Namen des Nutzers
		String nutzerName = myScanner.next();
		System.out.print("Bitte geben Sie Ihr Alter ein: ");
		// Die Variable nutzerAlter speichert das Alter des Nutzers
		int nutzerAlter = myScanner.nextInt();
		
		System.out.print("Ich lebe in Ihren W�nden " + nutzerName + " und zwar schon seit " +  nutzerAlter + " Jahren");
		myScanner.close();
	}

}
