
public class Aufgabe3 {

	public static void main(String[] args) {
		
		String s1 = "22.4234234";
		String s2 = "111.222";
		String s3 = "4.00";
		String s4 = "1000000.551";
		String s5 = "97.34";
		
		System.out.printf("|%.5s|\n", s1);
		System.out.printf("|%.6s|\n", s2);
		System.out.printf("|%4s|\n", s3);
		System.out.printf("|%.10s|\n", s4);
		System.out.printf("|%s|\n", s5);
		
	}

}
