package Uebung2;

public class Aufgabe3 {

	public static void main(String[] args) {
		
		//Fahrenheit links, Celsius rechts, auf 2 Nachkommastellen runden
		
		System.out.printf("%-11s"+ "|" + "%10s\n", "Fahrenheit", "Celsius");
		System.out.printf("-----------------------\n");
		System.out.printf("%-11.2f"+ "|" + "%10.2f\n", -20., -28.889);
		System.out.printf("%-11.2f"+ "|" + "%10.2f\n", -10., -23.3333);
		System.out.printf("%-+11.2f"+ "|" + "%10.2f\n", 0., -17.7778);
		System.out.printf("%-+11.2f"+ "|" + "%10.2f\n", 20., -6.6667);
		System.out.printf("%-+11.2f"+ "|" + "%10.2f\n", 30., -1.1111);
		
		
	}

}
