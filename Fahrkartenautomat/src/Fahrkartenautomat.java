﻿import java.util.Scanner;

class Fahrkartenautomat
{
	
	static Scanner tastatur = new Scanner(System.in);
	
    public static void main(String[] args)
    {
        while(true) {
        	double zuZahlenderBetrag = fahrkartenbestellungErfassen();
        	
        	double rueckgeldBetrag = fahrkartenBezahlen(zuZahlenderBetrag);
        	
        	rueckgeldAusgeben(rueckgeldBetrag);
        	
        	fahrkartenAusgeben();
        	
        	System.out.println("Neue Ticketbestellung aufgeben? [J]a/[N]ein");
        	char kundenWunsch = tastatur.next().charAt(0);
        	if(kundenWunsch== 'N') {
        		break;
        	}
        }
    	
    	
    	tastatur.close();
                          
    }
    
    //Bestellung mit gewünsten Tickets und Preis erfassen
	private static double fahrkartenbestellungErfassen() {
		
		
		//Aufgabe 1
		//Der Vorteil der Benutzung zweier Arrays für diesen Schritt an Stelle der vorherigen Lösung ist die einfache Änderbarkeit/Anpassbarkeit der Preise
		//Des weiteren hat man eine einfache Zuordnungsweise über einen gemeinsamen Index
		//Die Länge der Arrays ist leicht anpassbar und der Code funktioniert bei Änderung der Kartenzahl weiter
		double zwischenbetrag = 0.0;
		double zuZahlenderBetrag = 0.0;
		double[] auswahlPreis = {2.90, 3.30, 3.60, 1.90, 8.60, 9.00, 9.60, 23.50, 24.30, 24.90};
		String[] auswahlText = {
				"1: Einzelfahrschein Berlin AB",
				"2: Einzelfahrschein Berlin BC",
				"3: Einzelfahrschein Berlin ABC",
				"4: Kurzstrecke",
				"5: Tageskarte Berlin AB",
				"6: Tageskarte Berlin BC",
				"7: Tageskarte Berlin ABC",
				"8: Kleingruppen-Tageskarte Berlin AB",
				"9: Kleingruppen-Tageskarte Berlin BC",
				"10: Kleingruppen-Tageskarte Berlin ABC"
		};
		
		while(true) {
			
			System.out.println("Wählen Sie Ihre Wunschfahrkarte für Berlin AB aus:");
			for(int i = 0; i < auswahlPreis.length; i++) {
				System.out.printf("%s " + "%.2f\n", auswahlText[i], auswahlPreis[i]);
			}
			System.out.println("100: Ticketbestellung Abschließen");
			

				int eingabeTicket = tastatur.nextInt() - 1;
				double ticketPreis = 0.0;
				
				if(eingabeTicket < auswahlPreis.length && eingabeTicket >= 0) {
					ticketPreis += auswahlPreis[eingabeTicket];
					System.out.printf("%s" + "%d\n", "Ihre Wahl: ", eingabeTicket + 1);
				}else {
					if(eingabeTicket == 99) {
						return zuZahlenderBetrag;
					} else {
						System.out.printf("%s" + "%d" + "\n", "Ihre Wahl ", eingabeTicket + 1);
						System.out.println("Kein gültiges Ticket");
						continue;
					}
				}
				
//				switch(eingabeTicket) {
//					case(1) :
//						ticketPreis = 2.9;
//						System.out.println("Ihre Wahl: 1");
//						break;
//					case(2) :
//						System.out.println("Ihre Wahl: 2");
//						ticketPreis = 8.6;
//						break;
//					case(3) :
//						System.out.println("Ihre Wahl: 2");
//						ticketPreis = 23.5;
//						break;
//					case(9) : 
//						if(zuZahlenderBetrag > 0) {
//							System.out.println("Ihre Wahl: 9");
//							return zuZahlenderBetrag;
//						} else {
//							System.out.println("Sie haben noch keine Tickets ausgewählt.");
//							continue;
//						}
//
//					default : 
//						System.out.printf("%s" + "%d" + "\n", "Ihre Wahl ", eingabeTicket);
//						System.out.println("Kein gültiges Ticket");
//						continue;
//				}
			    //Eingabe der gewünschten Ticketanzahl
				while(true) {
					System.out.println("Anzahl der gewünschten Tickets: ");
				    int gewuenschteTickets = tastatur.nextInt();
				    
				    if (gewuenschteTickets > 0 && gewuenschteTickets < 11) {
				    	zwischenbetrag = gewuenschteTickets * ticketPreis;
				    	System.out.printf("%s" + "%d" + "\n", "Anzahl der Tickets: ", gewuenschteTickets);
				    	System.out.printf("%s" + "%.2f" + "\n", "Zwischensumme: ", zwischenbetrag);
				    	break;
				    } else {
				    	System.out.println("Wählen Sie eine Anzahl von 1 bis 10 Tickets aus.");
				    	continue;
				    }
				}	 
				
				zuZahlenderBetrag += zwischenbetrag;
			}
		
		//Aufgabe 3
		//Die neue Implementierung ist dynamischer, bei leicht veränderbarer Ticketzahl funktioniert der Code weiter
		//Allerdings kann es zu Fehlern kommen, wenn man beider Array unterschiedlich lang machen sollte
			
		}
	
//		while (true) {
//			//Eingabe des zu Zahlenden Betrages
//		    System.out.print("Zu zahlender Betrag (EURO): ");
//		    double ticketPreis = tastatur.nextDouble();
//		       
//		    //Eingabe der gewünschten Ticketanzahl
//		    System.out.println("Anzahl der gewünschten Tickets: ");
//		    double gewuenschteTickets = tastatur.nextByte();
//		    
//		    if (gewuenschteTickets > 0) {
//		    	double zuZahlenderBetrag = gewuenschteTickets * ticketPreis;
//		    	return zuZahlenderBetrag;
//		    } else {
//		    	continue;
//		    }
	
	
	
	//Bezahlen und Rückgeldberechnung
	private static double fahrkartenBezahlen(double zuZahlenderBetrag) {
		 
		double eingezahlterGesamtbetrag = 0.00;
		double restBetrag = 0.0;
		double rückgabebetrag = 0.0;
		
	    while(eingezahlterGesamtbetrag < zuZahlenderBetrag)
	       {
	    	   restBetrag = zuZahlenderBetrag - eingezahlterGesamtbetrag;
	    	   System.out.printf("%s" + "%.2f" + " EURO" + "\n","Noch zu zahlen: ", restBetrag);
	    	   System.out.print("Eingabe (mind. 5Ct, höchstens 50 Euro): ");
	    	   double eingeworfeneMünze = tastatur.nextDouble();
	    	   if (eingeworfeneMünze == 0.05 || eingeworfeneMünze == 0.1 || eingeworfeneMünze == 0.2 || eingeworfeneMünze == 0.5 || eingeworfeneMünze == 1 || eingeworfeneMünze == 2 || eingeworfeneMünze == 5 || eingeworfeneMünze == 10 || eingeworfeneMünze == 20 || eingeworfeneMünze == 50) {
	    		   eingezahlterGesamtbetrag += eingeworfeneMünze;
		           // Rückgeld berechnen
		           rückgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
	    	   } else {
	    		   System.out.println("Keine gültige Münze");
	    		   continue;
	    	   }
	           
	       }
	    return rückgabebetrag;
	}
	
    // Rückgeldberechnung und -Ausgabe
    // -------------------------------
	private static void rueckgeldAusgeben(double rückgabebetrag) {
		
		if(rückgabebetrag > 0.00)
			{
	    	   System.out.printf("%s" + "%.2f" + "%s\n", "Der Rückgabebetrag in Höhe von ", rückgabebetrag, " EURO");
	    	   System.out.println("wird in folgenden Münzen ausgezahlt:");
	    	   
	           while(rückgabebetrag >= 2.00) // 2 EURO-Münzen	           
	           {
	        	   muenzeAusgeben(2, "EURO");
	        	   rückgabebetrag -= 2;
	           }
	    	  // rückgabeFunktionalität(rückgabebetrag, 2)
	           while(rückgabebetrag >= 1.00) // 1 EURO-Münzen
	           {
	        	   muenzeAusgeben(1, "EURO");
	        	   rückgabebetrag -= 1;
	           }
	           while(rückgabebetrag >= 0.50) // 50 CENT-Münzen
	           {
	        	   muenzeAusgeben(50, "CENT");
	        	   rückgabebetrag -= 0.5;
	           }
	           while(rückgabebetrag >= 0.2) // 20 CENT-Münzen
	           {
	        	   muenzeAusgeben(20, "CENT");
	        	   rückgabebetrag -= 0.2;
	           }
	           while(rückgabebetrag >= 0.10) // 10 CENT-Münzen
	           {
	        	   muenzeAusgeben(10, "CENT");
	        	   rückgabebetrag -= 0.1;
	           }
	           while(rückgabebetrag >= 0.05)// 5 CENT-Münzen
	           {
	        	   muenzeAusgeben(5, "CENT");
	        	   rückgabebetrag -= 0.05;
	           }
	       }      
	}
	
	private static void fahrkartenAusgeben() {
		 // Fahrscheinausgabe
	       // -----------------
	       System.out.printf("\n\nFahrschein wird ausgegeben");
	       for (int i = 0; i < 8; i++)
	       {
	          System.out.print("=");
	         Fahrkartenautomat.warte(250);
	       }
	       
	       System.out.println("\n\nVergessen Sie nicht, Ihre/-n Fahrscheine/-n\n" + 
	    		   "vor Fahrtantritt entwerten zu lassen!\n"+
	               "Wir wünschen Ihnen und Ihren Begleitpersonen eine gute Fahrt.");
	}
	
	private static void warte(int wartezeit) {		
		 try {
				Thread.sleep(wartezeit);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	}
	
	private static void muenzeAusgeben(int betrag, String einheit) {
		
			System.out.printf("%d %s ", betrag, einheit);		
	}
	
//	private static void rückgabeFunktionalität(double rückgabebetrag, double muenzGroesse) {
//		while(rückgabebetrag >= muenzGroesse)
//        {
//     	   rückgabebetrag -= muenzGroesse;
//        }
//	}
}
		


